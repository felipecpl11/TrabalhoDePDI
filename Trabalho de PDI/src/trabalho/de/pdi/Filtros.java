/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.de.pdi;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.ImageIO;

/**
 *
 * @author felip_000
 */
public class Filtros {

    int[][] janelaR;
    int[][] janelaG;
    int[][] janelaB;
    int tamMascara;

    public Filtros() {
    }

    public Filtros(int valorMascara) {
        this.tamMascara = valorMascara;
        janelaR = new int[valorMascara][valorMascara];
        janelaG = new int[valorMascara][valorMascara];
        janelaB = new int[valorMascara][valorMascara];

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                janelaR[i][j] = 1;
                janelaG[i][j] = 1;
                janelaB[i][j] = 1;
            }
        }
    }

    public double[] FiltroMedia(int[][] janelaR, int[][] janelaG, int[][] janelaB) {
        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                somatorioR += janelaR[i][j];
                somatorioG += janelaG[i][j];
                somatorioB += janelaB[i][j];
            }
        }
        double[] RGB = new double[3];
        RGB[0] = somatorioR / (double) (tamMascara * tamMascara);
        RGB[1] = somatorioG / (double) (tamMascara * tamMascara);
        RGB[2] = somatorioB / (double) (tamMascara * tamMascara);

        return RGB;

    }

    public double[] FiltroMediana(ArrayList listaR, ArrayList listaG, ArrayList listaB) {
        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        Collections.sort(listaR);
        Collections.sort(listaG);
        Collections.sort(listaB);

        double[] RGB = new double[3];

        int posicao = (tamMascara - 1) / 2;
        RGB[0] = (int) listaR.get(posicao);
        RGB[1] = (int) listaG.get(posicao);
        RGB[2] = (int) listaB.get(posicao);

        return RGB;
    }

    public double[] FiltroModaTomCinza(int[][] janela) {
        ArrayList<Integer> valores = new ArrayList<Integer>();
        ArrayList<Integer> contador = new ArrayList<Integer>();
        
        int aux;
        int maisAparece;
        
        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                if(!valores.contains(janela[i][j])){
                    valores.add(janela[i][j]);
                    contador.add(1);
                }
                else{
                    aux = contador.get(valores.indexOf(janela[i][j]));
                    aux++;
                    contador.set(valores.indexOf(janela[i][j]), aux);
                }
            }
        }
        maisAparece = valores.get(0);
        for(int i=0; i< contador.size()-1; i++){
            if(contador.get(i+1) > contador.get(i))
                maisAparece = valores.get(i+1);
        }
        
        double[] RGB = new double[3];
        RGB[0] = maisAparece;
        RGB[1] = maisAparece;
        RGB[2] = maisAparece;

        return RGB;

    }

    public double[] FiltroBartlett(int[][] janelaR, int[][] janelaG, int[][] janelaB, boolean bConvolucao) {

        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        int[][] bartlett = {
            {1, 2, 3, 2, 1},
            {2, 4, 6, 4, 2},
            {3, 6, 9, 6, 3},
            {2, 4, 6, 4, 2},
            {1, 2, 3, 2, 1}};

        ArrayList convolucao = new ArrayList();

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                convolucao.add(bartlett[i][j]);
            }
        }

        Collections.reverse(convolucao);

        int[][] bartlettC = new int[5][5];
        int contador = 0;

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                bartlettC[i][j] = (int) convolucao.get(contador);
                contador++;
            }
        }

        if (bConvolucao == false) {
            for (int i = 0; i < tamMascara; i++) {
                for (int j = 0; j < tamMascara; j++) {
                    somatorioR += janelaR[i][j] * bartlett[i][j];
                    somatorioG += janelaG[i][j] * bartlett[i][j];
                    somatorioB += janelaB[i][j] * bartlett[i][j];
                }
            }
        } else {
            for (int i = 0; i < tamMascara; i++) {
                for (int j = 0; j < tamMascara; j++) {
                    somatorioR += janelaR[i][j] * bartlettC[i][j];
                    somatorioG += janelaG[i][j] * bartlettC[i][j];
                    somatorioB += janelaB[i][j] * bartlettC[i][j];
                }
            }
        }
        double[] RGB = new double[3];
        RGB[0] = somatorioR / (double) (81);
        RGB[1] = somatorioG / (double) (81);
        RGB[2] = somatorioB / (double) (81);

        return RGB;

    }

    public double[] FiltroGaussiano(int[][] janelaR, int[][] janelaG, int[][] janelaB) {

        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        int[][] gaussiano = {
            {1, 4, 6, 4, 1},
            {4, 16, 24, 16, 5},
            {6, 24, 36, 24, 6},
            {4, 16, 24, 16, 4},
            {1, 4, 6, 4, 1}};

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                somatorioR += janelaR[i][j] * gaussiano[i][j];
                somatorioG += janelaG[i][j] * gaussiano[i][j];
                somatorioB += janelaB[i][j] * gaussiano[i][j];
            }
        }
        double[] RGB = new double[3];
        RGB[0] = somatorioR / (double) (256);
        RGB[1] = somatorioG / (double) (256);
        RGB[2] = somatorioB / (double) (256);

        return RGB;

    }

    public double[] FiltroLaplaciano(int[][] janelaR, int[][] janelaG, int[][] janelaB) {

        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        int[][] laplace = {
            {1, 1, 1},
            {1, -8, 1},
            {1, 1, 1}};

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {
                somatorioR += janelaR[i][j] * laplace[i][j];
                somatorioG += janelaG[i][j] * laplace[i][j];
                somatorioB += janelaB[i][j] * laplace[i][j];
            }
        }
        double[] RGB = new double[3];

        if (somatorioR < 0 || somatorioG < 0 || somatorioB < 0) {
            RGB[0] = 0;
            RGB[1] = 0;
            RGB[2] = 0;
        } else if (somatorioR > 255 || somatorioG > 255 || somatorioB > 255) {
            RGB[0] = 255;
            RGB[1] = 255;
            RGB[2] = 255;
        } else {
            RGB[0] = somatorioR;
            RGB[1] = somatorioG;
            RGB[2] = somatorioB;
        }
        return RGB;

    }

    public double[] FiltroPrewitt(int[][] janelaR, int[][] janelaG, int[][] janelaB) {

        double somatorioR = 0;
        double somatorioG = 0;
        double somatorioB = 0;

        int[][] prewitt = {
            {-1, -1, -1},
            {0, 0, 0},
            {1, 1, 1}};
//            {-1, 0, 1},
//            {-1, 0, 1},
//            {-1, 0, 1}};

        for (int i = 0; i < tamMascara; i++) {
            for (int j = 0; j < tamMascara; j++) {

                somatorioR += janelaR[i][j] * prewitt[i][j];
                somatorioG += janelaG[i][j] * prewitt[i][j];
                somatorioB += janelaB[i][j] * prewitt[i][j];

            }
        }

        double[] RGB = new double[3];

        if (somatorioR < 0 || somatorioG < 0 || somatorioB < 0) {
            RGB[0] = 0;
            RGB[1] = 0;
            RGB[2] = 0;
        } else if (somatorioR > 255 || somatorioG > 255 || somatorioB > 255) {
            RGB[0] = 255;
            RGB[1] = 255;
            RGB[2] = 255;
        } else {
            RGB[0] = somatorioR;
            RGB[1] = somatorioG;
            RGB[2] = somatorioB;
        }
        return RGB;
    }

    public BufferedImage filhoHightBoost(BufferedImage imagemBorrada, BufferedImage imagemOriginal) throws IOException {

        BufferedImage novaImagem;

        int k = 2;

        novaImagem = imagemOriginal;

        int w = imagemOriginal.getWidth();
        int h = imagemOriginal.getHeight();

        Color cOriginal, cBorrada, RGB;
        RGB = cOriginal = cBorrada = null;

        int[][] mascaraR = new int[w][h];
        int[][] mascaraG = new int[w][h];
        int[][] mascaraB = new int[w][h];

        int R, G, B;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                cOriginal = new Color(imagemOriginal.getRGB(i, j));
                cBorrada = new Color(imagemBorrada.getRGB(i, j));

                mascaraR[i][j] = Math.abs(cOriginal.getRed() - cBorrada.getRed());
                mascaraG[i][j] = Math.abs(cOriginal.getGreen() - cBorrada.getGreen());
                mascaraB[i][j] = Math.abs(cOriginal.getBlue() - cBorrada.getBlue());

                R = cOriginal.getRed() + k * mascaraR[i][j];
                G = cOriginal.getGreen() + k * mascaraG[i][j];
                B = cOriginal.getBlue() + k * mascaraB[i][j];

                if (R > 255) {
                    R = 255;
                }
                if (G > 255) {
                    G = 255;
                }
                if (B > 255) {
                    B = 255;
                }

                RGB = new Color(R, G, B);
                novaImagem.setRGB(i, j, RGB.getRGB());
            }
        }

        return novaImagem;
//        for (int i = 0; i < w; i++) {
//            for (int j = 0; j < h; j++) {
//                cOriginal = new Color(imagemOriginal.getRGB(i, j));
//
//                mascaraR[i][j] = Math.abs(cOriginal.getRed() - cBorrada.getRed());
//                mascaraG[i][j] = Math.abs(cOriginal.getGreen() - cBorrada.getGreen());
//                mascaraB[i][j] = Math.abs(cOriginal.getBlue() - cBorrada.getBlue());
//            }
//        }

    }

}
